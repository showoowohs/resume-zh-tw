﻿#履歷表

---
##  基本資料
![this picture is me](https://github.com/showoowohs/Resume-zh-tw/blob/master/demo/IMG_2628.JPG)

|          |                        |
| -------- | ---------------------- |
| 姓名     | 林伯勳                 |
| 生日     | 1987/10/2              |
| 身高     | 178cm                  |        
| 體重     | 65kg                   |
| 手機     | 0927-507-098           |
| 居所     | 台北市內湖區           |

---
##  工作領域與技能
|             |                                           |
| ----------- | ----------------------                    |
| 程式語言    | C, C++, Java, Perl, Windows Batch Script, Linux Shell Script |
| 工作技能    |	Protocol : I2C, GPIO, UART 等             |
|             | Android : Linux BSP, Framework, Apps 等   | 
| 作業系統    | Windows Series: Windows 7, Windows Embedded 7, Windows POSready7 等|
|             | Linux Series : Ubuntu                     |
| 編譯器與工具| Visual Studio, Eclipse, Arduino, Vim, Git |
	
---
##  工作經驗與歷程
  
### 2012-2015 任職於宏奇科技股份有限公司擔任研發部軟體工程師
### 產品開發與維護
#### A. Windows 系列 ( Windows Embedded 7, Windows industry 8.1, POSready 7等 )
       1. 客製化 Windows Embedded OS
       2. 客製化 APP ( 透過 Inno Setup 製作安裝檔及使用 Batch Script 維護 )
       3. 開發與維護 Windows recovery 機制與功能 ( 透過 WinPE, Batch Script等 )
#### B. Android 系列 ( Platform: TI, MediaTek, Qualcomm 等 )
       1. BSP(Board Support Package) 整合維護與功能開發
         1.1. Porting driver and framework : 
                   LCM ( Toshiba tc35876x )
                   Touch ( Goodix gt9xx, EETI )
                   GPIO use
                   Ethernet ( DAVICOM DM9601 )
                   GPS ( u-blox ) 
                   3G/4G ( Telit, Sierra, u-blox LISA-u270 ) 
                   NFC ( Holy Stone) 
         1.2. Firmware Verify ( WiFi, Bluetooth, GPS 等 )
       2. Test APP/Tools develop
       3. System API Develop ( 提供 API 給 customer 使用 )
#### C. Linux 系列
       1. Smart Card Reader Integration
       2. Barcode scanner Integration
### 特別專案
       1. 長距離 WiFi 傳輸 Project (傳輸距離可到達 1 Km)
         1.1. 客製化 Windows Embedded 7 ( 安裝 OS 後, 同時也會安裝客戶的 App 與 driver )
         1.2. 客製化 Windows platform recovery solution ( 可透過 Hard Disk 或 CD  restore OS )
         1.3. 開發與維護 APP
       2. 長距離 Bluetooth 傳輸 Project ( 傳輸距離可到達 1 Km )
         2.1. 撰寫 client / server 端 APP
       3. 3D 腳踏車 Project
         3.1. 透過 Android 的 RS232 或 Bluetooth 控制腳踏車上的 MCU ( LCM 畫面中的 bike 會與實體 bike 的動作一致)
       4. 使用 smart battery on Android
       5. Fix Qualcomm msm8960 WiFi bug(Xperia Z, WMM)
       6. 監獄 Project
         6.1. 開發特殊 Ethernet APP ( 讓 customer 可透過 APP 更換 domain ,DNS 等 )
       7. 投票機 Project
         7.1. 客製化專屬 APP  ( 透過 I2C 控制 IO Board 上的電源, 達到省電效果 )
       8. Android hotkey 功能 ( 可重新定義 Android default key 功能 )
       9. 於 Android 上切換 OTG/Host mode (透過 GPIO)

___
### 2010-2012 就讀樹德科技大學資訊工程所
#### 助教與產學合作
       1. 於2011擔任 台灣優奎士有限公司part time
       2. 於2011擔任 互動式多媒體系統專題研究
       3. 於2011曾參與隆盈科技股份有限公司OBD Project
       4. 於2011-2012擔任 JAVA程式設計助教
       5. 於2012擔任 智慧型行車資訊系統設計助教
       6. 於2012參與 南台科技大學『世紀民生CAN EVB CS8959』計劃
       7. 於2012參與 邁世通實業股份有限公司，教育部『智慧生活無重校園電子書包』製作
#### 比賽事蹟
       1. 於2011報名 中華電信「2011電信創新應用大賽」
       2. 於2011參加 建國百年開放軟體創作大賽(第二名)
       3. 於2011參加 嵌入式系統(ES)設計競賽(設計完成獎)
       4. 於2012報名 第二屆資旺盃 Android 程式設計競賽
       5. 於2012參加 經濟部樂活百年飆創意比賽(佳作)
       6. 於2012受邀參加 『發掘智慧金礦．打造科技未來』—2011年全國技專校院最新研發成果發表記者會
       7. 於2012報名 大學院校智慧電子系統設計大賽
       8. 於2012參加 第二屆「智慧感知與辨識」領域專題成果發表觀摩會
       9. 於2012參加 2012德州儀器亞洲區DSP暨MCU應用競賽
#### 論文發表
       1. 於2012參加2012E-CASE & e-Tech 2012香港研討會(報告paper題目: The Implementation of Vehicle Driving Safety Support System Based on Android Platform)
       2. 於2012參加第二屆網路智能與應用研討會(朝陽科大發表paper題目: 基於Android平台的車輛維修導引與評價度建議之車載診斷系統)

___
### 2006-2010 就讀和春技術學院資訊工程系
#### 特殊經歷
       1. 於2009擔任吉他社活動長
       2. 於2009擔任資訊工程系之系會活動長
       3. 於2010協助系上完成 縮短鄉城計畫(教小孩學電腦)

---
## 興趣
   使用 3C 產品
   健身
